
public class ElseIf {

	
	public static void main(String[] args) {
		
		if (2 > 3) {
			System.out.println(":)");
		}
		else {
			System.out.println(":(");
		}
		
		System.out.println((4 < 5) ? ":)" : ":(");
		
		
	}
}
