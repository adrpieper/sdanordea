
public class TypyDanych {

	public static void main(String[] args) {


		int i = 4;
		System.out.println("i = " + i);
		byte b = 127;
		System.out.println("b = " + b);
		short s = 1;
		System.out.println("s = " + s);
		long l = 10000000000000000L;
		System.out.println("l = " + l);
		double d = 0.5;
		System.out.println("d = " + d);
		float f = 2.0f;
		System.out.println("f = " + f);
		
		char c = 'z';
		System.out.println("c = " + c);
		boolean bool = false;
		System.out.println("bool = " + bool);
		
		String text = "Hello World!";
		System.out.println(text);
	}

}
