
public class TablicaImion {

	public static void main(String[] args) {
		String[] imiona = {"Jan", "Adam", "Ewa", "Angelika" };

		for(int i = 0 ; i < imiona.length ; i++) {
			System.out.println(imiona[i]);
		}
		
		for(int i = 0 ; i < imiona.length ; i+= 2) {
			
			System.out.println(imiona[i]);
		}
		
		for(int i = 0 ; i < imiona.length ; i++) {
			if (i%2 == 0) {
				System.out.println(imiona[i]);
			}
		}
		
		for(int i = 0 ; i < imiona.length ; i++) {
			if (imiona[i].startsWith("A")) {
				System.out.println(imiona[i]);
			}
		}
		
		for(String imie : imiona) {
			if (imie.startsWith("A")) {
				System.out.println(imie);
			}
		}
	}

}
