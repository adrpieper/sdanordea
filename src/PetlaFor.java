import java.util.Scanner;

public class PetlaFor {

	public static void main(String[] args) {

		System.out.println("Od 1 do 10");
		for(int i = 1 ; i <= 10 ; i++) {
			System.out.println(i);
		}

		System.out.println("Parzyste 1 do 20");
		for(int i = 1 ; i <= 20 ; i++) {
			if (i % 2 == 0) { 
				System.out.println(i);
			}
		}
		
		for(int i = 2 ; i <= 20 ; i+=2) {
			System.out.println(i);
		}
		
		for(char c = 'a'; c <= 'z' ; c++) {
			System.out.println(c);
		}
		
		for (int i = 0 ; i < 6 ; i++) {
			System.out.println("Hello World!");
		}
		
		
		Scanner scanner = new Scanner(System.in);
		int a;
		do{
			System.out.println("Hello World!");
			a = scanner.nextInt();
		}while(a>0);
		System.out.println("Koniec");
	}
	
	

}
