import java.util.Scanner;

public class TablicaOdUzytkownika {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Podaj rozmiar tablicy.");
		int rozmiar = scanner.nextInt();
		
		int[] liczby = new int[rozmiar];
		
		for(int i = 0 ; i < liczby.length; i++) {

			System.out.println("Podaj element tablicy.");
			liczby[i] = scanner.nextInt(); 
		}

		
		for(int liczba : liczby) {
			System.out.println(liczba);
		}

		// To jest r�wnowa�ne p�tli for
		for(int i = 0 ; i < liczby.length; i++) {
			int liczba = liczby[i];
			System.out.println(liczba);
		}
	}

}
